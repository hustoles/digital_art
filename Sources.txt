Textures:

Exit Bricks - https://ambientcg.com/view?id=Bricks090
Chair - https://ambientcg.com/view?id=Wood028
Concrete - https://ambientcg.com/view?id=Concrete030
Floor - https://ambientcg.com/view?id=Carpet016
Floor 2 - https://ambientcg.com/view?id=WoodFloor054
Walls - https://www.sharetextures.com/textures/wall/wallpaper-10
walls2 - https://ambientcg.com/view?id=Wallpaper001A
Ceiling - https://ambientcg.com/view?id=OfficeCeiling001
Lamps - https://ambientcg.com/view?id=OfficeCeiling004
Locker Metal - https://ambientcg.com/view?id=Metal038
Wooden Door - https://poly.cam/tools/ai-texture-generator (Prompt: light wood with grain and smooth surface)
Table: https://ambientcg.com/view?id=Fabric029
Wood Props: https://ambientcg.com/view?id=Wood028


Sounds:
Ambient - https://freesound.org/people/Resaural/sounds/626096/
Abgrund - https://freesound.org/people/hanneswannerberger/sounds/275628/
Phone ring - https://freesound.org/people/mycompasstv/sounds/456433/
Walking - https://freesound.org/people/morganpurkis/sounds/384636/
Monster - https://freesound.org/people/Fosdork/sounds/705469/