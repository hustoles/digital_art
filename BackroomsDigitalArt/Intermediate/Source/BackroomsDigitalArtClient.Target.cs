using UnrealBuildTool;

public class BackroomsDigitalArtClientTarget : TargetRules
{
	public BackroomsDigitalArtClientTarget(TargetInfo Target) : base(Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V3;
		IncludeOrderVersion = EngineIncludeOrderVersion.Latest;
		Type = TargetType.Client;
		ExtraModuleNames.Add("BackroomsDigitalArt");
	}
}
